package com.pino.task;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.pino.domain.InsaAttachVO;
import com.pino.mapper.InsaAttachMapper;

import lombok.Setter;
import lombok.extern.log4j.Log4j;

@Log4j
@Component
public class FileCheckTask {
	
	@Setter(onMethod_ = { @Autowired })
	private InsaAttachMapper attachMapper;
	
	// 어제 날짜 폴더 구하기 ========================================================================
	private String getFolderYesterDay() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		Calendar cal = Calendar.getInstance();
		
		cal.add(Calendar.DATE, -1);
		
		String str = sdf.format(cal.getTime());
		
		return str.replace("-", File.separator);
	}
	
	@Scheduled(cron="0 0 2 * * *")
	public void checkFiles()throws Exception{
		
		log.warn("file check task run .....................");
		log.warn(new Date());
		
		// DB의 파일목록. 어제 날짜 구해서 목록에 넣음
		List<InsaAttachVO> fileList = attachMapper.getOldFiles();
		
		// 폴더안의 파일을 DB파일 리스트랑 비교할 준비
		List<Path> fileListPaths = fileList.stream().map(vo -> Paths.get("C:\\upload", vo.getUploadPath(),vo.getUuid()+"_"+vo.getFileName())).collect(Collectors.toList());
		
		// 이미지 파일이면 썸네일 가져와라
		fileList.stream().filter(vo -> vo.isFileType() == true).map(vo -> Paths.get("C:\\upload", vo.getUploadPath(),"s_"+vo.getUuid()+"_"+vo.getFileName())).forEach(p -> fileListPaths.add(p));
		
		log.warn("=========================================================");
		
		fileListPaths.forEach(p -> log.warn(p));
		
		// 어제자 폴더의 파일
		File targetDir = Paths.get("C:\\upload", getFolderYesterDay()).toFile();
		
		// DB에 존재하지 않는 파일 목록 구하기 (삭제할 목록구하기)
		File[] removeFiles = targetDir.listFiles(file -> fileListPaths.contains(file.toPath()) == false);
		
		log.warn("-------------------------------------------");
		
		for(File file : removeFiles) {
			file.delete();
		}
		
	}
}
