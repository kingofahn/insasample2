package com.pino.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.pino.domain.InsaVO;
import com.pino.service.InsaService;

import lombok.AllArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j;
@Controller
@Log4j
@AllArgsConstructor
public class IdController {
	@Setter(onMethod_ = @Autowired)
	private InsaService insaService;
	
	@ResponseBody
	@RequestMapping(value = "/idCheck", method = RequestMethod.POST)
	public int postIdCheck(HttpServletRequest req) throws Exception {
		// 아이디 체크 Ajax로 넘겨줌 =========================
		log.info("post idCheck =========");
		
		String id = req.getParameter("id");
		InsaVO idCheck = insaService.idCheck(id);
		
		// 일치하는 값이 없으면 중복이 아니다
		int result = 0;
		
		if(idCheck != null) {
			// 널이 아니면 값이 있으므로 중복이다
			result = 1;
		}
		
		return result;
	}
}
