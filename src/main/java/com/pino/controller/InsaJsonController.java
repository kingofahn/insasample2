package com.pino.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.pino.domain.Criteria;
import com.pino.domain.InsaVO;
import com.pino.service.InsaService;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

@RestController
@Log4j
@AllArgsConstructor
public class InsaJsonController {
	
	private InsaService insaService;
	
	// jqGrid를 위한 json 데이터 목록 가져오기 =====================================================================
	@RequestMapping(value = "/list", produces= {MediaType.APPLICATION_JSON_UTF8_VALUE})
	@ResponseBody
	public ResponseEntity<List<InsaVO>> getList(Criteria cri) throws Exception{
		log.info(insaService.selectInsaAll(cri));
		return new ResponseEntity<>(insaService.selectInsaAll(cri), HttpStatus.OK);
	}
	
}
