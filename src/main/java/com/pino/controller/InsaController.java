package com.pino.controller;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.pino.domain.Criteria;
import com.pino.domain.InsaAttachVO;
import com.pino.domain.InsaVO;
import com.pino.domain.PageDTO;
import com.pino.service.InsaComService;
import com.pino.service.InsaService;

import lombok.AllArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j;

@Controller
@Log4j
@RequestMapping("/")
@AllArgsConstructor
public class InsaController {
	
	@Setter(onMethod_ = @Autowired)
	private InsaService insaService;
	
	@Setter(onMethod_ = @Autowired)
	private InsaComService insaComService;
	
	//메인으로 이동 =================================================================== 
	@GetMapping("/index.do")
	public String goIndex() throws Exception {
		log.info("main화면으로 이동=================");
		return "/insa/index";
	}
	
	//입력페이지로 이동하라 ===================================================================
	@GetMapping("/insaInputForm.do")
	public String goInsaInputForm(Model model) throws Exception {
		log.info("인사입력페이지로 이동 ================");
		model.addAttribute("insaCom",insaComService.selectCom());
		log.info(insaComService.selectCom()+"=========================");
		return "/insa/insaInputForm";
	}
	
	//입력페이지에서 입력하라 ===================================================================
	@PostMapping("/insaInputForm.do")
	public String InsaInput(InsaVO insaVo, RedirectAttributes rttr) throws Exception {
		log.info("insavo : " + insaVo +"================");
		
		if(insaVo.getAttachList() != null) {
			insaVo.getAttachList().forEach(attach -> log.info(attach));
		}
		
		log.info("===================================");
		
		// 파일첨부기능 추가로 주석처리해둠
		insaService.insertInsa(insaVo);
		rttr.addFlashAttribute("result",insaVo.getSabun());
		
		return "redirect:/insaListForm.do";
	}
	
	//인사리스트페이지로 이동 =================================================
	@GetMapping("/insaListForm.do")
	public String goInsaListForm(Criteria cri, Model model) throws Exception {
		log.info("인사리스트페이지로 이동 =================");
		model.addAttribute("resultList",insaService.selectInsa(cri));
//		model.addAttribute("pageMaker", new PageDTO(cri, 123));
		
		int total = insaService.getTotalCount(cri);
		
		model.addAttribute("pageMaker", new PageDTO(cri, total));
		
//		model.addAttribute("pgc",insaComService.selectPGCName());
//		log.info("[[[[[[[[[[[[[[["+insaComService.selectPGCName()+"=========================");
		
		// name의 이름을 바꿔서 다시 해보기 ==========================
		
		return "/insa/insaListForm";
	}
	
	// 인사정보 한건조회 ==================================================	
	@GetMapping({"/insaUpdateForm.do"})
	public String goInsaUpdateForm(@RequestParam("sabun") int sabun,@ModelAttribute("cri") Criteria cri, Model model) throws Exception {
		log.info("사번 [" + sabun + "]의 인사정보 조회 페이지로 이동 =============");
		model.addAttribute("getInsa",insaService.selectInsaBySabun(sabun));
		model.addAttribute("insaCom",insaComService.selectCom());
		return "/insa/insaUpdateForm";
	}
	
	// 인사정보 수정 ====================================================
	@PostMapping("/insaUpdateForm.do")
	public String insaUpdate(InsaVO insaVo,@ModelAttribute("cri") Criteria cri,RedirectAttributes rttr) throws Exception {
		log.info("수정 : " + insaVo + "=================");
		
		if(insaService.updateInsa(insaVo)) {
			rttr.addFlashAttribute("result", "update");
		}
		
		rttr.addAttribute("pageNum",cri.getPageNum());
		rttr.addAttribute("amount",cri.getAmount());
		
		return "redirect:/insaListForm.do";
	}
	
	
	// 삭제시 파일도 함께 삭제되는 함수 =================================================================
	private void deleteFiles(List<InsaAttachVO> attachList) {
		if(attachList == null || attachList.size() == 0) {
			//첨부파일 없으면 작업하지 마라
			return;
		}
		log.info("delete Attach files =================");
		log.info(attachList);
		
		attachList.forEach(attach ->{
			try {
				Path file = Paths.get("C:\\upload\\"+attach.getUploadPath()+"\\"+attach.getUuid()+"_"+attach.getFileName());
				
				Files.deleteIfExists(file);
				
				if(Files.probeContentType(file).startsWith("image")) {
					Path thumbNail = Paths.get("C:\\upload\\"+attach.getUploadPath()+"\\s_"+attach.getUuid()+"_"+attach.getFileName());
					
					Files.delete(thumbNail);
				}
			} catch (Exception e) {
				log.error("delete file error" + e.getMessage());
			}
		});
	}
	
	
	// 인사정보 삭제 ===================================================
	@PostMapping("/insaDeleteForm.do")
	public String deleteInsa(int sabun, @ModelAttribute("cri") Criteria cri, RedirectAttributes rttr) throws Exception {
		log.info("remove...." + sabun);
		
		List<InsaAttachVO> attachList = insaService.getAttachList(sabun);
		
		if(insaService.deleteInsa(sabun)) {
			// 첨부파일도 삭제해라
			deleteFiles(attachList);
			
			rttr.addFlashAttribute("result","delete");
		}
		
//		rttr.addAttribute("pageNum",cri.getPageNum());
//		rttr.addAttribute("amount",cri.getAmount());
		
		return "redirect:/insaListForm.do" + cri.getListLink();
	}
	
	//첨부파일 리스트 가져오기  =================================================================
	@GetMapping(value="/getAttachList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public ResponseEntity<List<InsaAttachVO>> getAttachList(int sabun) throws Exception{
		log.info("getAttachList : " + sabun);
		return new ResponseEntity<>(insaService.getAttachList(sabun), HttpStatus.OK);
	}
	
}
