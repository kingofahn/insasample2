package com.pino.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.tika.Tika;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import com.pino.controller.UploadController;
import com.pino.domain.AttachFileDTO;

import lombok.extern.log4j.Log4j;
import net.coobird.thumbnailator.Thumbnailator;

@Controller
@Log4j
public class UploadController {
	// form을 이용한 파일 업로드 방식 ============================================================
	@GetMapping("/uploadForm")
	public void uploadForm() {
		log.info("upload Form 을 찾아가라");
	}
	
	@PostMapping("/uploadFormAction")
	public void uploadFormPost(MultipartFile[] uploadFile, Model model) throws Exception {
		
		String uploadFolder = "C:\\upload";
		
		for (MultipartFile multipartFile : uploadFile) {
			log.info("-----------------------------------");
			log.info("패러미터의 이름 : " + multipartFile.getName());
			log.info("파일이름 : " + multipartFile.getOriginalFilename());
			log.info("파일크기 : " + multipartFile.getSize());
			log.info("파일 존재여부 : " + multipartFile.isEmpty());
			
			// 위에 테스트 로그들이 제대로 인식이 된다면 저장을 한다
			File saveFile = new File(uploadFolder, multipartFile.getOriginalFilename());
			
			try {
				multipartFile.transferTo(saveFile);
			}catch(Exception e) {
				log.error(e.getMessage());
			}//end catch
			
		}// end for
	}
	
	// ajax을 이용한 파일 업로드 방식 ============================================================
	@GetMapping("/uploadAjax")
	public void uploadAjax() throws Exception {
		log.info("upload ajax 로 가라!!!");
	}
	
	// 테스트로 Controller 파일에 넣었지만 나중에는 util같은 폴더를 만들어서 클래스를 따로 빼주는 것이 좋다
	// 폴더 가져옴 =====================================================
	private String getFolder() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyy-MM-dd");
		Date date = new Date();
		String str = sdf.format(date);
		return str.replace("-", File.separator);
	}
	
	
	// 이미지인지 체크하는 함수 생성 =====================================================
	private boolean checkImageType(File file) {
		try {
			//String contentType=Files.probeContentType(file.toPath());
			// probeContentType 는 버그가 있다. 파일의 실제 속성을 알아내는게 아니라 확장자를 기준으로 돌아간다
			
			Tika tika = new Tika();
			
			String contentType = tika.detect(file.toPath());
			
			return contentType.startsWith("image");
		}catch(IOException e) {
			e.printStackTrace();
		}
		return false;
	}
	@PostMapping(value="/uploadAjaxAction",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public ResponseEntity<List<AttachFileDTO>> uploadAjaxPost(MultipartFile[] uploadFile) throws Exception {
		
		List<AttachFileDTO> list = new ArrayList<>();
		String uploadFolder = "C:\\upload";
		
		// c:\\upload\\2018\\12\\13 형태로 폴더 생성 ===================================================
		String uploadFolderPath = getFolder();
		File uploadPath = new File(uploadFolder, uploadFolderPath);
		
		if(uploadPath.exists() == false) {
			// getFloder를 확인했을때 일치하는 파일이 없으면 만들어라
			uploadPath.mkdirs();
		}
		// yyy/MM/dd 폴더 만들어라
		
		for (MultipartFile multipartFile : uploadFile) {
			AttachFileDTO attachDTO = new AttachFileDTO();
			
			log.info("-----------------------------------");
			log.info("패러미터의 이름 : " + multipartFile.getName());
			log.info("파일이름 : " + multipartFile.getOriginalFilename());
			
			String uploadFileName = multipartFile.getOriginalFilename();
			
			//IE 는 파일 이름 변경해줘야함
			uploadFileName = uploadFileName.substring(uploadFileName.lastIndexOf("\\") + 1);
			log.info("IE때문에 패치한 파일명 : " + uploadFileName);
			
			attachDTO.setFileName(uploadFileName);
			
			// uuid 사용해서 파일 이름 정규화
			UUID uuid = UUID.randomUUID();
			uploadFileName = uuid.toString() + "_" + uploadFileName;
			
			// 위에 테스트 로그들이 제대로 인식이 된다면 저장을 한다
			//File saveFile = new File(uploadFolder, uploadFileName);
			//File saveFile = new File(uploadPath, uploadFileName);
			
			try {
				
				File saveFile = new File(uploadPath, uploadFileName);
				multipartFile.transferTo(saveFile);
				
				attachDTO.setUuid(uuid.toString());
				attachDTO.setUploadPath(uploadFolderPath);
				
				//check image type file
				if(checkImageType(saveFile)) {
					attachDTO.setImage(true);
					
					FileOutputStream thumbnail = new FileOutputStream(new File(uploadPath, "s_"+uploadFileName));
					Thumbnailator.createThumbnail(multipartFile.getInputStream(), thumbnail, 150, 180);
					
					thumbnail.close();
				}
				list.add(attachDTO);
			}catch(Exception e) {
				log.error(e.getMessage());
			}//end catch
		}// end for
		return new ResponseEntity<>(list, HttpStatus.OK);
	}
	
	@GetMapping("/display")
	@ResponseBody
	public ResponseEntity<byte[]> getFile(String fileName) throws Exception {
		log.info("파일명 : "+ fileName);
		File file = new File("c:\\upload\\"+fileName);
		
		log.info("file: " +file);
		
		ResponseEntity<byte[]> result = null;
		
		try {
			HttpHeaders header = new HttpHeaders();
			//http 프로토콜을 이용해서 파일을 전송할 때 해더부분에 Content-type을 기록
			//Content-Type 을 일반적으로 MIME타입 이라고 함.
			
			header.add("Content-Type", Files.probeContentType(file.toPath()));
			result=new ResponseEntity<>(FileCopyUtils.copyToByteArray(file),header, HttpStatus.OK);
			
		}catch(IOException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	@GetMapping(value="/download", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	@ResponseBody
	public ResponseEntity<Resource> downloadFile(@RequestHeader("User-Agent")String userAgent,String fileName) throws Exception {
		log.info(userAgent+"=================================================================");
		
		log.info("다운된 파일 : " + fileName);
		
		Resource resource = new FileSystemResource("c:\\upload\\"+fileName);
		
		// 해당경로에 파일이 없으면, NOT_FOUND
		if(resource.exists() == false) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		String resourceName = resource.getFilename();
		// _ 언더바의 위치를 구해서 잘라서 uuid를 제외시킨 파일명을 넣어줌
		// remove UUID
		String resourceOriginalName = resourceName.substring(resourceName.indexOf("_") + 1);
		
		HttpHeaders headers = new HttpHeaders();
		try {
			String downloadName = null;
			
			if( userAgent.contains("Trident")) {
				log.info("여기는 IE입니다===========");
				downloadName = URLEncoder.encode(resourceOriginalName, "UTF-8").replaceAll("\\+"," ");
			}else if(userAgent.contains("Edge")) {
				log.info("여기는 Edge브라우저 입니다===========");
				downloadName = URLEncoder.encode(resourceOriginalName, "UTF-8");
			}else {
				log.info("여기는 Chrome브라우저 입니다 =============");
				downloadName = new String(resourceOriginalName.getBytes("UTF-8"),"ISO-8859-1");
			}
			
			log.info("downloadName : " + downloadName);
			// 한굴이 깨져서 저장되는것을 방지한다
			headers.add("Content-Disposition","attachment; filename=" + downloadName);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return new ResponseEntity<Resource>(resource, headers, HttpStatus.OK);
	}
	
	@PostMapping("/deleteFile")
	@ResponseBody
	public ResponseEntity<String> deleteFile(String fileName, String type) throws Exception {
		log.info("deleteFile : " + fileName);
		
		File file;
		
		try {
			file = new File("c:\\upload\\"+ URLDecoder.decode(fileName, "UTF-8"));
			
			file.delete();
			if(type.equals("image")) {
				String largeFileName = file.getAbsolutePath().replace("s_", "");
				log.info("largeFileName : " + largeFileName);
				file = new File(largeFileName);
				file.delete();
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<String>("삭제되었습니다.",HttpStatus.OK);
		
	}
}
