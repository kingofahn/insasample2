package com.pino.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pino.domain.InsaComVO;
import com.pino.mapper.InsaComMapper;

import lombok.Setter;
import lombok.extern.log4j.Log4j;

@Log4j
@Service
public class InsaComServiceImpl implements InsaComService{
	
	@Setter(onMethod_ = @Autowired)
	private InsaComMapper insaComMapper;
	
	// 직위구분코드 가져오기 ==========
	@Override
	public List<InsaComVO> selectPGCName() {
		log.info("직위구분 코드 가져오기 ");
		return insaComMapper.selectPGCName();
	}

	@Override
	public List<InsaComVO> selectCom() {
		return insaComMapper.selectCom();
	}
}
