package com.pino.service;

import java.util.List;

import com.pino.domain.InsaComVO;

public interface InsaComService {
	// 직위구분코드 가져오기 ==========
	public List<InsaComVO> selectPGCName();
	
	//전체 직위 코드 가져오기 ==========
	public List<InsaComVO> selectCom();
	
}
