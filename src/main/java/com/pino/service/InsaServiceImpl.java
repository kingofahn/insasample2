package com.pino.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pino.domain.Criteria;
import com.pino.domain.InsaAttachVO;
import com.pino.domain.InsaVO;
import com.pino.mapper.InsaAttachMapper;
import com.pino.mapper.InsaMapper;

import lombok.Setter;
import lombok.extern.log4j.Log4j;

@Log4j
@Service
public class InsaServiceImpl implements InsaService{
	
	@Setter(onMethod_ = @Autowired)
	private InsaMapper insaMapper;
	
	@Setter(onMethod_ = @Autowired)
	private InsaAttachMapper attachMapper;
	
	// 인사정보 입력 ==================================================
	@Transactional
	@Override
	public void insertInsa(InsaVO insaVo) throws Exception {
		log.info(insaVo.toString()+" 인사정보 입력하라 ===========");
		
		insaMapper.insertSelectKey(insaVo);
		
		if (insaVo.getAttachList() == null || insaVo.getAttachList().size() <= 0) {
			return;
		}

		insaVo.getAttachList().forEach(attach -> {
			attach.setSabun(insaVo.getSabun());
			attachMapper.insert(attach);
			// 두테이블에 모두 인서트해라
		});
	}
	
	// 아이디체크 ==================================================
	@Override
	public InsaVO idCheck(String id) throws Exception {
		return insaMapper.idCheck(id);
	}
	
	// 인사정보 목록 ==================================================
	@Override
	public List<InsaVO> selectInsa(Criteria cri) throws Exception {
		log.info("인사목록 가져오기 ==============");
		return insaMapper.selectInsa(cri);
	}

	// 인사정보 한건조회 ==================================================	
	@Override
	public InsaVO selectInsaBySabun(int sabun) throws Exception {
		log.info("사번 [" + sabun + "]의 인사정보 조회 =============");
		return insaMapper.selectInsaBySabun(sabun);
	}
	
	// 인사정보 수정 ==================================================
	@Transactional
	@Override
	public boolean updateInsa(InsaVO insaVo) throws Exception {
		log.info("인사정보 수정 임플 : "+ insaVo +"=============");
		
		attachMapper.deleteAll(insaVo.getSabun());

		boolean modifyResult = insaMapper.updateInsa(insaVo) == 1;
		
		int attachListSize = 0;
		
		if(insaVo.getAttachList() != null) {
			attachListSize = insaVo.getAttachList().size();
		}
		
		if(modifyResult && attachListSize > 0) {
			insaVo.getAttachList().forEach(attach -> {
				attach.setSabun(insaVo.getSabun());
				attachMapper.insert(attach);
			});
		}

		return modifyResult;
	}
	
	// 인사정보 삭제 ==================================================
	@Transactional
	@Override
	public boolean deleteInsa(int sabun) throws Exception {
		log.info("인사정보 삭제 임플 : "+ sabun +"=============");
		
		attachMapper.deleteAll(sabun);
		
		return insaMapper.deleteInsa(sabun) == 1;
	}

	// 페이징을 위한 총게시물 개수  ==================================================
	@Override
	public int getTotalCount(Criteria cri) throws Exception {
		log.info("총 인사 명수 =============================");
		return insaMapper.getTotalCount(cri);
	}

	// 게시물의 조회와 첨부파일 ====================================================
	@Override
	public List<InsaAttachVO> getAttachList(int sabun) throws Exception {
		log.info("get Attach list by sabun" + sabun);
		return attachMapper.findBySabun(sabun);
	}
	
	// jqGrid용 인사====================================================
	@Override
	public List<InsaVO> selectInsaAll(Criteria cri) throws Exception {
		return insaMapper.selectInsaAll(cri);
	}

}
