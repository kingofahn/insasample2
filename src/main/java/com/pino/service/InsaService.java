package com.pino.service;

import java.util.List;

import com.pino.domain.Criteria;
import com.pino.domain.InsaAttachVO;
import com.pino.domain.InsaVO;

public interface InsaService {
	// 인사정보 입력
	public void insertInsa(InsaVO insaVo) throws Exception;
	
	// 아이디체크
	public InsaVO idCheck(String id) throws Exception;
	
	// 인사정보 목록
	public List<InsaVO> selectInsa(Criteria cri) throws Exception;
	
	// 인사정보 목록 jqGrid용
	public List<InsaVO> selectInsaAll(Criteria cri) throws Exception;
	
	// 인사정보 한건 조회
	public InsaVO selectInsaBySabun(int sabun) throws Exception;
	
	// 인사정보 수정
	public boolean updateInsa(InsaVO insaVo) throws Exception;
	
	// 인사정보 삭제
	public boolean deleteInsa(int sabun) throws Exception;

	// 페이징을 위한 총게시물 개수
	public int getTotalCount(Criteria cri) throws Exception;
	
	// 게시물의 조회와 첨부파일
	public List<InsaAttachVO> getAttachList(int sabun) throws Exception;
	
}
