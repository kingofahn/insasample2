package com.pino.domain;

import lombok.Data;

@Data
public class InsaComVO {
	private String gubun;
	private String code;
	private String name;
	private String note;
}
