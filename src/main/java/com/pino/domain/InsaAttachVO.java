package com.pino.domain;

import lombok.Data;

@Data
public class InsaAttachVO {

  private String uuid;
  private String uploadPath;
  private String fileName;
  private boolean fileType;
  
  private int sabun;
  private String gubun;
  
}
