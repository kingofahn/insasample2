package com.pino.domain;

import java.util.List;

import lombok.Data;

@Data
public class InsaVO {
	private int sabun;
	private String join_day;
	private String retire_day;
	private String put_yn;
	private String name;
	
	private String reg_no;
	private String eng_name;
	private String phone;
	private String hp;
	private String carrier;
	
	private String pos_gbn_code;
	private String cmp_reg_no;
	private String cmp_reg_image;
	private String sex;
	private int years;
	
	private String email;
	private String zip;
	private String addr1;
	private String addr2;
	private String dept_code;
	
	private String join_gbn_code;
	private String id;
	private String pwd;
	private int salary;
	private String kosa_reg_yn;
	
	private String kosa_class_code;
	private String mil_yn;
	private String mil_type;
	private String mil_level;
	private String mil_startdate;
	
	private String mil_enddate;
	private String job_type;
	private String gart_level;
	private String self_intro;
	private String crm_name;
	
	private String profile;
	
	private List<InsaAttachVO> attachList;
	private InsaComVO insaComName;

}
