package com.pino.domain;

import org.springframework.web.util.UriComponentsBuilder;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Criteria {
	private int sabun;
	private String name;
	private String join_gbn_code;
	private String put_yn;
	private String pos_gbn_code;
	private String join_day;
	private String retire_day;
	private String job_type;
	
	
	private int pageNum;
	private int amount;
	
	private String type;
	private String keyword;
	
	public Criteria() {
		this(1,10);
	}
	
	public Criteria(int pageNum, int amount) {
		this.pageNum = pageNum;
		this.amount = amount;
	}
	
	public String[] getTypeArr() {
		return type == null? new String[] {}: type.split("");
	}
	
	public String getListLink() {
		UriComponentsBuilder builder = UriComponentsBuilder.fromPath("").queryParam("pageNum", this.pageNum).queryParam("amount", this.amount).queryParam("type", this.type).queryParam("keyword", this.keyword);
		
		return builder.toUriString();
	}
}
