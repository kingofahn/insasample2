package com.pino.domain;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class PageDTO {
	private int startPage;
	private int endPage;
	private boolean prev, next;
	
	private int total;
	private Criteria cri;
	private int realEndPage;//실제마지막페이지
	
	public PageDTO(Criteria cri, int total) {
		this.cri = cri;
		this.total = total;
		
		// 10.0을 사용하는 이유는 정수를 정수로 나눴을때 정수이기때문에 실수처리 하기 위해 실수형태로 적어줌
		this.endPage = (int) (Math.ceil(cri.getPageNum() / 10.0)) * 10;
		this.startPage = this.endPage - 9;
		
		// 실제 끝페이지 > ceiling > 반올림을 하는것 > 전체 글에서 한페이지당 보여지는 글을 나눈다음 나머지가 있으면 하나 올려주는것
		int realEnd = (int) (Math.ceil((total * 1.0) / cri.getAmount()));
		this.realEndPage = realEnd;
		
		
		// 실제 끝페이지 끝페이지(endPage)보다 작으면 보정해라
		if (realEnd < this.endPage) {
			this.endPage = realEnd;
		}
		
		// 시작페이지가 1보다 크면 prev는 true
		this.prev = (this.startPage > 1);
		// 끝페이지가 실제끝페이지보다 작으면 next는 true
		this.next = (this.endPage < realEnd);
	}
	
}


