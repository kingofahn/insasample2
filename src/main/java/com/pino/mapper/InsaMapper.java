package com.pino.mapper;

import java.util.List;

import com.pino.domain.Criteria;
import com.pino.domain.InsaVO;

public interface InsaMapper {
	// 인사정보 입력
	public void insertInsa(InsaVO insaVo) throws Exception;
	
	// 인사정보 입력 키를저장
	public void insertSelectKey(InsaVO insaVo) throws Exception;
	
	// 아이디체크
	public InsaVO idCheck(String id) throws Exception;
	
	// 인사정보 목록
	public List<InsaVO> selectInsa(Criteria cri) throws Exception;
	
	// 인사정보 목록 jqGrid용
	public List<InsaVO> selectInsaAll(Criteria cri) throws Exception;
	
	// 인사정보 한건 조회
	public InsaVO selectInsaBySabun(int sabun) throws Exception;
	
	// 인사정보 수정
	public int updateInsa(InsaVO insaVo) throws Exception;
	
	// 인사정보 삭제
	public int deleteInsa(int sabun) throws Exception;
	
	// 페이징을 위한 총게시물 개수
	public int getTotalCount(Criteria cri) throws Exception;
	
}
