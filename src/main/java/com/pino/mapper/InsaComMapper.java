package com.pino.mapper;

import java.util.List;

import com.pino.domain.InsaComVO;

public interface InsaComMapper {
	// 직위구분코드 가져오기 ==========
	public List<InsaComVO> selectPGCName();
	
	// 직위구분코드 가져오기 ==========
	public List<InsaComVO> selectCom();
	

}
