package com.pino.mapper;

import java.util.List;

import com.pino.domain.InsaAttachVO;

public interface InsaAttachMapper {
	
	// 입력
	public void insert(InsaAttachVO vo);

	// 삭제
	public void delete(String uuid);
	
	//사번으로 불러오기
	public List<InsaAttachVO> findBySabun(int sabun);
	
	// 첨부파일 전부 삭제
	public void deleteAll(int sabun);
	
	// 옛날 파일 목록 불러오기 // 배치프로그램사용을위해 
	public List<InsaAttachVO> getOldFiles();
}
