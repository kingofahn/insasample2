<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>인사시스템</title>
	
	<!-- bootstrp setting -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
	<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	
	<link type="text/css" rel="stylesheet" href="resources/css/index.css" />
</head>
<body>
	<!-- Navbar -->
	<nav class="navbar">
	  <div class="container">
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>                        
	      </button>
	      <a class="navbar-brand" href="index.do">PINOSOFT</a>
	    </div>
	    <div class="collapse navbar-collapse" id="myNavbar">
	      <ul class="nav navbar-nav navbar-right">
	        <li><a href="index.do">Home</a></li>
	        <li><a href="/insaInputForm.do">인사입력</a></li>
	        <li><a href="/insaListForm.do">인사조회</a></li>
	      </ul>
	    </div>
	  </div>
	</nav>
	
	<!-- First Container -->
	<div class="first_content container-fluid bg-1 text-center">
	  	<h3 class="margin">인사관리 시스템</h3>
	  	<div class="p_wrap">
			<p>인사정보를 입력하겠습니다.</p> 
			<a href="/insaInputForm.do" class="btn btn-default">입력</a>
		</div>
		<div class="p_wrap">
			<p>인사정보를 조회하겠습니다.</p> 
			<a href="/insaListForm.do" class="btn btn-default">조회</a>
		</div>
	</div>
	<!-- Footer -->
	<footer class="container-fluid bg-4 text-center">
		<p>피노소프트 | Tel: 02-6925-2288 | Fax: 02-852-1848</p> 
		<p>주소: 서울시 금천구 가산동 60-44 이앤씨드림타워7차 3층 306호 | E-mail : pino6481@daum.net, dsk0749@hanmail.net</p>	
		<p>Copyright ⓒ ㈜ 피노소프트 All rights reserved.</p>
	</footer>
</body>
</html>