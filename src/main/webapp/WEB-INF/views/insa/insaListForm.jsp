<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>직원 리스트</title>
	
	<!-- bootstrp setting -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
	<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	
    <!-- jqGrid사용을 위한 CDN ===================== -->
	<link rel="stylesheet" type="text/css" media="screen" href="/resources/css/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="/resources/css/ui.jqgrid.css" />
    <script type="text/javascript" src="/resources/js/i18n/grid.locale-kr.js"></script>
    <script type="text/javascript" src="/resources/js/jquery.jqGrid.min.js"></script>
    
    <!-- List Page Style Sheet -->
    <link type="text/css" rel="stylesheet" href="/resources/css/insaListForm.css" />
	
	<script>
	$( function() {
		$( "#retire_day,#join_day" ).datepicker({
		 dateFormat: 'yy-mm-dd'
		});
	});
	</script>
</head>
<body>
	<!-- Navbar -->
	<nav class="navbar navbar-default">
	  <div class="container">
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>                        
	      </button>
	      <a class="navbar-brand" href="index.do">PINOSOFT</a>
	    </div>
	    <div class="collapse navbar-collapse" id="myNavbar">
	      <ul class="nav navbar-nav navbar-right">
	        <li><a href="insaInputForm.do">입력하기</a></li>
	        <li><a href="insaListForm.do">조회하기</a></li>
	      </ul>
	    </div>
	  </div>
	</nav>
	<div class="container col-sm-12 sub_content">
		<h2>직원 리스트</h2>
       	<form id="listForm" name="listForm" method="get">
       		<div class="col-sm-3 padding">
				<div class="col-sm-3 text_area">사번</div>
				<div class="col-sm-9"><input type="text" class="form-control" id="sabun" name="sabun"></div>
			</div>
			<div class="col-sm-3 padding">
				<div class="col-sm-3 text_area">성명</div>
				<div class="col-sm-9"><input type="text" class="form-control" id="name" name="name"></div>
			</div>
			<div class="col-sm-3 padding">
				<div class="col-sm-3 text_area">입사구분</div>
				<div class="col-sm-9">
					<select class="form-control" id="join_gbn_code" name="join_gbn_code">
						<option value="">(선택)</option>
						<option value="0">신입</option>
						<option value="1">경력</option>
					</select>
				</div>
			</div>
			<div class="col-sm-3 padding">
				<div class="col-sm-3 text_area">투입여부</div>
				<div class="col-sm-9">
					<select class="form-control" id="put_yn" name="put_yn">
						<option value="">(선택)</option>
						<option value="1">투입중</option>
						<option value="0">대기중</option>
					</select>
				</div>
			</div>
			<div class="col-sm-3 padding">
				<div class="col-sm-3 text_area">직위</div>
				<div class="col-sm-9">
					<select class="form-control" id="pos_gbn_code" name="pos_gbn_code">
						<option value="">(선택)</option>
						<option value="0001">사원</option>
						<option value="0002">대리</option>
						<option value="0003">과장</option>
						<option value="0004">차장</option>
						<option value="0005">부장</option>
						<option value="0006">사장</option>
					</select>
				</div>
			</div>
			<div class="col-sm-3 padding">
				<div class="col-sm-3 text_area">입사일자</div>
				<div class="col-sm-9">
					<input type="text" class="form-control" id="join_day" name="join_day" autocomplete=off>
				</div>
			</div>
			
			<div class="col-sm-3 padding">
				<div class="col-sm-3 text_area">퇴사일자</div>
				<div class="col-sm-9">
					<input type="text" class="form-control" id="retire_day" name="retire_day" autocomplete=off>
				</div>
			</div>
			
			<div class="col-sm-3 padding">
				<div class="col-sm-3 text_area">직종분류</div>
				<div class="col-sm-9">
					<select class="form-control" id="job_type" name="job_type">
						<option value="">(선택)</option>
						<option value="1">직종01</option>
						<option value="2">직종02</option>
					</select>
				</div>
			</div>
			
			<div class="col-sm-12 padding text-right">
				<a href="#" id="searchBtn" class="btn btn-primary">검색</a>
			</div>
	    </form>
		
		<div class="col-sm-12 result_box">
			<table id="jqGrid"></table>
			<div id="jqGridPager"></div>
			
			<!-- jqGrid사용을 위해 원래 테이블 주석처리 ======================= -->
			<%-- 
			<table>
				<tr>
					<th>사번</th>
					<th>성명</th>
					<th>주민번호</th>
					<th>연락처</th>
					<th>직위</th>
					<th>입사일</th>
					<th>퇴사일</th>
					<th>투입여부</th>
					<th>연봉</th>
					<th>상세보기</th>
				</tr>
				<c:choose>
				    <c:when test="${empty resultList}">
				    	<tr class="insa_list_tr">
				        	<td colspan="10">검색된 항목이 없습니다.</td>
				        </tr>
				    </c:when>
				    <c:otherwise>
				    	<c:forEach var="pgc" items="${ pgc }">
				    		<tr>
				    			<td colspan="10">
				    				<c:out value="${ pgc.name }" />
				    			</td>
				    		</tr>
				    	</c:forEach>
						<c:forEach var="insa" items="${ resultList }" >
		                	<tr class="insa_list_tr">
		                		<td><c:out value="${ insa.sabun }" /></td>
		                		<td><c:out value="${ insa.name }" /></td>
		                		<td>
									<c:choose>
										<c:when test="${!empty insa.reg_no}">
											<c:out value="${ insa.reg_no }" />
										</c:when>
										<c:when test="${empty insa.reg_no}">
											-
										</c:when>							
									</c:choose>
		                		</td>
		                		<td><c:out value="${ insa.hp }" /></td>
		                		<td>
		                			<c:choose>
										<c:when test="${!empty insa.pos_gbn_code}">
											<c:if test="${insa.pos_gbn_code eq '0001'}">
												사원
											</c:if>
											<c:if test="${insa.pos_gbn_code eq '0002'}">
												대리
											</c:if>
											<c:if test="${insa.pos_gbn_code eq '0003'}">
												과장
											</c:if>
										</c:when>
										<c:when test="${empty insa.pos_gbn_code}">
											-
										</c:when>							
									</c:choose>
		                		</td>
		                		<td>
		                			<c:choose>
										<c:when test="${!empty insa.join_day}">
											<fmt:parseDate value='${insa.join_day}' var='join_day_trad' pattern='yyyymmdd'/>
											<fmt:formatDate value="${join_day_trad}" pattern="yyyy-MM-dd"/>
											<c:set var="join_day_str" value="${ insa.join_day}" />
											${fn:substring(join_day_str,0,10) }
										</c:when>
										<c:when test="${empty insa.join_day}">
											-
										</c:when>							
									</c:choose>
		                		</td>
		                		<td>
		                			<c:choose>
										<c:when test="${!empty insa.retire_day}">
											<fmt:parseDate value='${insa.retire_day}' var='retire_day_trad' pattern='yyyymmdd'/>
											<fmt:formatDate value="${retire_day_trad}" pattern="yyyy-MM-dd"/>
											<c:set var="retire_day_str" value="${ insa.retire_day}" />
											${fn:substring(retire_day_str,0,10) }
											
										</c:when>
										<c:when test="${empty insa.retire_day}">
											-
										</c:when>							
									</c:choose>
		                		</td>
		                		<td>
		                			<c:choose>
										<c:when test="${!empty insa.put_yn}">
											<c:if test="${insa.put_yn eq '0'}">
												투입중
											</c:if>
											<c:if test="${insa.put_yn eq '1'}">
												대기중
											</c:if>
										</c:when>
										<c:when test="${empty insa.put_yn}">
											-
										</c:when>							
									</c:choose>
		                		</td>
		                		<td>
		                			<c:choose>
										<c:when test="${!empty insa.salary}">
											<c:out value="${ insa.salary}" />
										</c:when>
										<c:when test="${empty insa.salary}">
											-
										</c:when>							
									</c:choose>
		                		</td>
		                		<td>
		                			<a href="<c:out value="${ insa.sabun }" />" class="btn btn-primary btn-xs move">상세보기</a>
		                		</td>
		                	</tr>
		                </c:forEach>
				    </c:otherwise>
				</c:choose>
			</table>
			<div class="pagging_nav">
				<ul>
					<c:if test="${pageMaker.prev }">
						<li class="prev">
							<a href="${ pageMaker.startPage - 1 }">prev</a>
						</li>
					</c:if>
					
					<c:forEach var="num" begin="${ pageMaker.startPage }" end="${ pageMaker.endPage }">
						<li class='pageNum ${ pageMaker.cri.pageNum == num ? "active":"" }'>
							<a href="${ num }">${ num }</a>
						</li>
					</c:forEach>
					
					<c:if test="${pageMaker.next }">
						<li class="next">
							<a href="${ pageMaker.endPage + 1 }">next</a>
						</li>
					</c:if>
				</ul>
			</div>
			 --%>
		</div>
	</div>
	
	<form id="actionForm" action="/insaListForm.do" method="get">
		<input type="hidden" name="pageNum" value="${ pageMaker.cri.pageNum }" />
		<input type="hidden" name="amount" value="${ pageMaker.cri.amount }" />
	</form>
	
	<script src="resources/js/insaListForm.js"></script>
	<script>
		// 수정, 삭제 후 확인창 띄우기 =================================================
		var result = '<c:out value="${result}"/>';
		
		console.log("결과값있나?" + result);
		
		if(result == "update"){
			alert("수정이 완료되었습니다.");
		}
		if(result == "delete"){
			alert("삭제가 완료되었습니다.");
		}
	</script>
</body>
</html>
