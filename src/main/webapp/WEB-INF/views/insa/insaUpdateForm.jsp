<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c"         uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"      uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring"    uri="http://www.springframework.org/tags"%>   
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>인사관리시스템</title>
	
	<!-- bootstrp setting -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link type="text/css" rel="stylesheet" href="/resources/css/insaUpdateForm.css" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
	<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>

</head>

<body>
	<!-- Navbar -->
	<nav class="navbar navbar-default">
	  <div class="container">
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>                        
	      </button>
	      <a class="navbar-brand" href="index.do">PINOSOFT</a>
	    </div>
	    <div class="collapse navbar-collapse" id="myNavbar">
	      <ul class="nav navbar-nav navbar-right">
	        <li><a href="insaInputForm.do">입력하기</a></li>
	        <li><a href="insaListForm.do">조회하기</a></li>
	      </ul>
	    </div>
	  </div>
	</nav>
	
	<div class="container col-sm-12 sub_content">
		<h2>직원 상세 정보 수정</h2>

		<div class="col-sm-3 padding text-center profile_box">
			<div class='uploadResult'>
				<ul id="profile_result">
					
				</ul>
			</div>
			<input type="file" name="uploadFile" />
			<button type="button" class="btn btn-default" id="profile"><i class="fas fa-camera"></i> 사진올리기</button>
		</div>

		<form id="updateForm" name="updateForm" method="post">
			<input type="hidden" name="profile" value="pf" />
			<input type="hidden" name="pageNum" value="${ cri.pageNum }" />
	        <input type="hidden" name="amount" value="${ cri.amount }" />
			<input type="hidden" name="sabun" value="${getInsa.sabun}" />
			<ul class="hidden_li_box"></ul>
		    <div class="col-sm-3 padding">
				<div class="col-sm-3 text_area">* 사번</div>
				<div class="col-sm-9"><input type="text" class="form-control" id="sabun" value="<c:out value='${getInsa.sabun }' />" readonly /></div>
				<div class="col-sm-12 warning_text"><p></p></div>
			</div>
			<div class="col-sm-3 padding">
				<div class="col-sm-3 text_area">* 한글성명</div>
				<div class="col-sm-9"><input type="text" value="<c:out value='${getInsa.name }' />" class="form-control" id="name" name="name" /></div>
				<div class="col-sm-12 warning_text"><p>이름을 입력해주세요</p></div>
			</div>
			<div class="col-sm-3 padding">
				<div class="col-sm-3 text_area">영문성명</div>
				<div class="col-sm-9"><input type="text" value="<c:out value='${getInsa.eng_name }' />" class="form-control" id="eng_name" name="eng_name" /></div>
				<div class="col-sm-12 warning_text"><p></p></div>
			</div>
			<div class="col-sm-3 padding">
				<div class="col-sm-3 text_area">* ID</div>
				<div class="col-sm-9">
					<input type="text" value="<c:out value='${getInsa.id }' />" class="form-control" id="id" name="id" />
					<!-- <div class="input-group-btn">
						<input class="btn btn-default" type="button" value="중복체크" id="idCheck">
					</div> -->
				</div>
				<div class="col-sm-12 warning_text"><p>아이디는 필수 입력값입니다</p></div>
			</div>
			<div class="col-sm-3 padding">
				<div class="col-sm-3 text_area">* PW</div>
				<div class="col-sm-9">
					<!-- <input type="text" class="form-control" onkeyup="insertPass();" id="pwd_box" name="pwd_box">
					<input type="hidden" id="pwd" name="pwd" > -->
					<input type="password" value="<c:out value='${getInsa.pwd }' />" class="form-control" id="pwd" name="pwd" readonly />
				</div>
				<div class="col-sm-12 warning_text"><p>패스워드는 필수 입력값입니다</p></div>
			</div>
			<div class="col-sm-3 padding">
				<div class="col-sm-3 text_area">* PW 확인</div>
				<div class="col-sm-9">
					<!-- <input type="text" class="form-control" onkeyup="insertPass();" id="pwd_check_box" name="pwd_check_box">
					<input type="hidden" id="pwd_check" name="pwd_check" > -->
					<input type="password" value="<c:out value='${getInsa.pwd }' />" class="form-control" id="pwd_check" name="pwd_check" readonly />
				</div>
				<div class="col-sm-12 warning_text"><p>패스워드가 일치하지 않습니다</p></div>
			</div>
			<div class="col-sm-3 padding">
				<div class="col-sm-3 text_area">전화번호</div>
				<div class="col-sm-9"><input type="text" value="<c:out value='${getInsa.phone }' />" class="form-control" id="phone" name="phone" onkeyup="autoHypenPhone()" placeholder="-를 제외하고 입력해주세요" /></div>
				<div class="col-sm-12 warning_text"><p></p></div>
			</div>
			<div class="col-sm-3 padding">
				<div class="col-sm-3 text_area">*핸드폰번호</div>
				<div class="col-sm-9"><input type="text" value="<c:out value='${getInsa.hp }' />" class="form-control" id="hp" name="hp" onkeyup="autoHypenHp()" placeholder="-를 제외하고 입력해주세요" /></div>
				<div class="col-sm-12 warning_text"><p>폰번호는 필수 입력값입니다.</p></div>
			</div>
			<div class="col-sm-3 padding">
				<div class="col-sm-3 text_area">주민번호</div>
				<div class="col-sm-9">
					<!-- <input type="text" class="form-control" onkeyup="insertPass();" id="reg_no_box" name="reg_no_box">
					<input type="hidden" id="reg_no" name="reg_no"> -->
					<input type="password" value="<c:out value='${getInsa.reg_no }' />" class="form-control" id="reg_no" name="reg_no" readonly />
				</div>
				<div class="col-sm-12 warning_text"><p></p></div>
			</div>
			<div class="col-sm-3 padding">
				<div class="col-sm-3 text_area">연령</div>
				<div class="col-sm-9"><input type="text" value="<c:out value='${getInsa.years }' />" class="form-control" id="years" name="years" /></div>
				<div class="col-sm-12 warning_text"><p></p></div>
			</div>
			<div class="col-sm-3 padding">
				<div class="col-sm-3 text_area">*이메일</div>
				<div class="col-sm-9">
					<input type="text" value="<c:out value='${getInsa.email }' />" class="form-control" id="email" name="email" />
				</div>
				<div class="col-sm-12 warning_text"><p>이메일은 필수 입력값입니다.</p></div>
			</div>
			<div class="col-sm-3 padding">
				<div class="col-sm-3 text_area">직종체크</div>
				<div class="col-sm-9">
					<select class="form-control" id="job_type" name="job_type">
						<option value="">(선택)</option>
						<option value="1">직종01</option>
						<option value="2">직종02</option>
						<option value="3">직종03</option>
					</select>
				</div>
				<div class="col-sm-12 warning_text"><p></p></div>
			</div>
			
			<div class="col-sm-9 padding">
				<div class="col-sm-1 text_area">주소</div>
				<div class="col-sm-11 form-inline text-left address_box">
					<input type="text" value="<c:out value='${getInsa.zip }' />" class="form-control" id="zip" name="zip" placeholder="우편번호" readonly />
					<input type="button" class="form-control" onclick="sample6_execDaumPostcode()" value="주소검색">
					<input type="text" value="<c:out value='${getInsa.addr1 }' />" class="form-control" id="addr1" name="addr1" placeholder="주소" />
					<input type="text" value="<c:out value='${getInsa.addr2 }' />" class="form-control" id="addr2" name="addr2" placeholder="상세주소" />
				</div>
				<div class="col-sm-12 warning_text"><p></p></div>
			</div>
			
			<div class="col-sm-3 padding">
				<div class="col-sm-3 text_area">성별</div>
				<div class="col-sm-9">
					<select value="<c:out value='${getInsa.sex }' />" class="form-control" id="sex" name="sex">
						<option value="">(선택)</option>
						<option value="0">여자</option>
						<option value="1">남자</option>
					</select>
				</div>
				<div class="col-sm-12 warning_text"><p></p></div>
			</div>
			
			<div class="col-sm-3 padding">
				<div class="col-sm-3 text_area">부서</div>
				<div class="col-sm-9">
					<select class="form-control" id="dept_code" name="dept_code">
						<option value="">(선택)</option>
						<c:forEach var="insaCom" items="${insaCom }" >
				        	<c:if test="${insaCom.gubun eq 'DC' }">
				        		<option value="${ insaCom.code }">${ insaCom.name }</option>
				        	</c:if>
				        </c:forEach>
					</select>
				</div>
				<div class="col-sm-12 warning_text"><p></p></div>
			</div>
			
			<div class="col-sm-3 padding">
				<div class="col-sm-3 text_area">연봉(만원)</div>
				<div class="col-sm-9">
					<input type="text" value="<c:out value='${getInsa.salary }' />" class="form-control" id="salary" placeholder="(만원)" name="salary" />
				</div>
				<div class="col-sm-12 warning_text"><p></p></div>
			</div>
			
			<div class="col-sm-3 padding">
				<div class="col-sm-3 text_area">입사구분</div>
				<div class="col-sm-9">
					<select value="<c:out value='${getInsa.join_gbn_code }' />" class="form-control" id="join_gbn_code" name="join_gbn_code">
						<option value="">(선택)</option>
						<option value="0">신입</option>
						<option value="1">경력</option>
					</select>
				</div>
				<div class="col-sm-12 warning_text"><p></p></div>
			</div>
			
			<div class="col-sm-3 padding">
				<div class="col-sm-3 text_area">입사일자</div>
				<div class="col-sm-9">
					<c:set var="join_day_str" value="${ getInsa.join_day}" />
					<input type="text" value="${fn:substring(join_day_str,0,10) }" class="form-control" id="join_day" name="join_day" autocomplete=off>
				</div>
				<div class="col-sm-12 warning_text"><p></p></div>
			</div>
			
			<div class="col-sm-3 padding">
				<div class="col-sm-3 text_area">퇴사일자</div>
				<div class="col-sm-9">
					<c:set var="retire_day_str" value="${ getInsa.retire_day}" />
					<input type="text" value="${fn:substring(retire_day_str,0,10) }" class="form-control" id="retire_day" name="retire_day" autocomplete=off>
				</div>
				<div class="col-sm-12 warning_text"><p></p></div>
			</div>
			
			<div class="col-sm-3 padding">
				<div class="col-sm-3 text_area">직위</div>
				<div class="col-sm-9">
					<select class="form-control" id="pos_gbn_code" name="pos_gbn_code">
						<option value="">(선택)</option>
						<c:forEach var="insaCom" items="${insaCom }" >
				        	<c:if test="${insaCom.gubun eq 'PGC' }">
				        		<option value="${ insaCom.code }">${ insaCom.name }</option>
				        	</c:if>
				        </c:forEach>
					</select>
				</div>
				<div class="col-sm-12 warning_text"><p></p></div>
			</div>
			
			<div class="col-sm-3 padding">
				<div class="col-sm-3 text_area">등급</div>
				<div class="col-sm-9">
					<select class="form-control" id="gart_level" name="gart_level">
						<option value="">(선택)</option>
						<option value="gl01">등급01</option>
						<option value="gl02">등급02</option>
						<option value="gl03">등급03</option>
					</select>
				</div>
				<div class="col-sm-12 warning_text"><p></p></div>
			</div>
			
			<div class="col-sm-3 padding">
				<div class="col-sm-3 text_area">투입여부</div>
				<div class="col-sm-9">
					<select class="form-control" id="put_yn" name="put_yn">
						<option value="">(선택)</option>
						<c:forEach var="insaCom" items="${insaCom }" >
				        	<c:if test="${insaCom.gubun eq 'PYN' }">
				        		<option value="${ insaCom.code }">${ insaCom.name }</option>
				        	</c:if>
				        </c:forEach>
					</select>
				</div>
				<div class="col-sm-12 warning_text"><p></p></div>
			</div>
			
			<div class="col-sm-3 padding">
				<div class="col-sm-3 text_area">군필여부</div>
				<div class="col-sm-9">
					<select class="form-control" id="mil_yn" name="mil_yn">
						<option value="">(선택)</option>
						<option value="yes">군필</option>
						<option value="no">미필</option>
					</select>
				</div>
				<div class="col-sm-12 warning_text"><p></p></div>
			</div>
			
			<div class="col-sm-3 padding">
				<div class="col-sm-3 text_area">군별</div>
				<div class="col-sm-9">
					<select class="form-control" id="mil_type" name="mil_type">
						<option value="">(선택)</option>
						<c:forEach var="insaCom" items="${insaCom }" >
				        	<c:if test="${insaCom.gubun eq 'MT' }">
				        		<option value="${ insaCom.code }">${ insaCom.name }</option>
				        	</c:if>
				        </c:forEach>
					</select>
				</div>
				<div class="col-sm-12 warning_text"><p></p></div>
			</div>
			
			<div class="col-sm-3 padding">
				<div class="col-sm-3 text_area">계급</div>
				<div class="col-sm-9">
					<select class="form-control" id="mil_level" name="mil_level">
						<option value="">(선택)</option>
						<c:forEach var="insaCom" items="${insaCom }" >
				        	<c:if test="${insaCom.gubun eq 'ML' }">
				        		<option value="${ insaCom.code }">${ insaCom.name }</option>
				        	</c:if>
				        </c:forEach>
					</select>
				</div>
				<div class="col-sm-12 warning_text"><p></p></div>
			</div>
			
			<div class="col-sm-3 padding">
				<div class="col-sm-3 text_area">입영일자</div>
				<div class="col-sm-9">
					<c:set var="mil_startdate_str" value="${ getInsa.mil_startdate}" />
					<input type="text" value="${fn:substring(mil_startdate_str,0,10) }" class="form-control" id="mil_startdate" name="mil_startdate" autocomplete=off />
				</div>
				<div class="col-sm-12 warning_text"><p></p></div>
			</div>
			<div class="col-sm-3 padding">
				<div class="col-sm-3 text_area">전역일자</div>
				<div class="col-sm-9">
					<c:set var="mil_enddate_str" value="${ getInsa.mil_enddate}" />
					<input type="text" value="${fn:substring(mil_enddate_str,0,10) }" class="form-control" id="mil_enddate" name="mil_enddate" autocomplete=off />
				</div>
				<div class="col-sm-12 warning_text"><p></p></div>
			</div>
			
			<div class="col-sm-3 padding">
				<div class="col-sm-3 text_area">KOSA등록</div>
				<div class="col-sm-9">
					<select class="form-control" id="kosa_reg_yn" name="kosa_reg_yn">
						<option value="">(선택)</option>
						<option value="yes">등록</option>
						<option value="no">미등록</option>
					</select>
				</div>
				<div class="col-sm-12 warning_text"><p></p></div>
			</div>
			
			<div class="col-sm-3 padding">
				<div class="col-sm-3 text_area">KOSA등급</div>
				<div class="col-sm-9">
					<select class="form-control" id="kosa_class_code" name="kosa_class_code">
						<option value="">(선택)</option>
						<c:forEach var="insaCom" items="${insaCom }" >
				        	<c:if test="${insaCom.gubun eq 'KS' }">
				        		<option value="${ insaCom.code }">${ insaCom.name }</option>
				        	</c:if>
				        </c:forEach>
					</select>
				</div>
				<div class="col-sm-12 warning_text"><p></p></div>
			</div>
			 
			<div class="col-sm-3 padding">
				<div class="col-sm-3 text_area">사업자번호</div>
				<div class="col-sm-9"><input type="text" value="<c:out value='${getInsa.cmp_reg_no }' />" class="form-control" id="cmp_reg_no" name="cmp_reg_no" /></div>
				<div class="col-sm-12 warning_text"><p></p></div>
			</div>
			
			<div class="col-sm-3 padding">
				<div class="col-sm-3 text_area">업체명</div>
				<div class="col-sm-9"><input type="text" value="<c:out value='${getInsa.crm_name }' />"  class="form-control" id="crm_name" name="crm_name" /></div>
				<div class="col-sm-12 warning_text"><p></p></div>
			</div>
			
			<div class="col-sm-6 padding">
				<div class="col-sm-2 text_area">사업자등록증</div>
				<div class="col-sm-10 form-inline text-left">
					<input type="text" class="form-control" id="cmp_reg_image_text" readonly>
					<input type="button"class="form-control" data-toggle="modal" data-target="#cmpModal" value="미리보기">
					<input type="button" id="cmp_reg_image" class="form-control" value="등록">
				</div>
				<div class="col-sm-12 warning_text"><p></p></div>
			</div>
			<!-- Modal -->
			<div class="modal fade" id="cmpModal" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">사업자등록증</h4>
						</div>
						<div class="modal-body">
							<ul class="cmp_reg_image_ul">
							
							</ul>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">Close</button>
						</div>
					</div>

				</div>
			</div>
			
			<div class="col-sm-6 padding">
				<div class="col-sm-2 text_area">자기소개</div>
				<div class="col-sm-10"><textarea value="<c:out value='${getInsa.self_intro }' />" class="form-control" rows="1" name="self_intro"></textarea></div>
				<div class="col-sm-12 warning_text"><p></p></div>
			</div>
			
			<div class="col-sm-6 padding">
				<div class="col-sm-2 text_area">이력서</div>
				<div class="col-sm-10 form-inline text-left">
					<input type="text" class="form-control" id="carrier_text" readonly>
					<input type="button" class="form-control" data-toggle="modal" data-target="#carrierModal" value="미리보기">
					<input type="button" id="carrier" class="form-control" value="파일업로드">
				</div>
				<div class="col-sm-12 warning_text"><p></p></div>
			</div>
			
			<!-- Modal -->
			<div class="modal fade" id="carrierModal" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">이력서</h4>
						</div>
						<div class="modal-body">
							<ul class="carrier_ul">
							
							</ul>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">Close</button>
						</div>
					</div>

				</div>
			</div>
			
			<div class="col-sm-12 padding text-right btn_box">
				<input type="button" id="submit_btn" class="btn btn-primary" value="수정" />
				<input type="button" id="insaDeleteBtn" class="btn btn-danger" value="삭제" />
				<input type="button" id="listBtn" class="btn btn-default" value="이전" />
			</div>
		</form>
	</div> 

	<script src="/resources/js/insaUpdateForm.js"></script>
	<script src="/resources/js/insaUpdateFormAjax.js"></script>
	<script>	
	$(document).ready(function(){
		(function(){
			var sabun = '<c:out value="${getInsa.sabun}" />';
			$.getJSON("/getAttachList", {sabun:sabun}, function(arr){
				console.log(arr);
				
				var str = "";
				
				$(arr).each(function(i, attach){
			          
				    //image type
				    if(attach.fileType){
						var fileCallPath =  encodeURIComponent( attach.uploadPath+ "/s_"+attach.uuid +"_"+attach.fileName);
						
						str += "<li data-path='"+attach.uploadPath+"' data-uuid='"+attach.uuid+"' "
						str +=" data-filename='"+attach.fileName+"' data-type='"+attach.fileType+"' data-gubun='"+attach.gubun+"' >";
						str += "<span> "+ attach.fileName+"</span>";
						str += "<button type='button' title='삭제' class='imgDeleteBtn' data-file=\'"+fileCallPath+"\' data-type='image' ";
						str += "><i class='fa fa-times'></i></button>";
						str += "<img src='/display?fileName="+fileCallPath+"'>";
						str +"</li>";
						
				    }else{
				    	var fileCallPath =  encodeURIComponent( obj.uploadPath+"/"+ obj.uuid +"_"+obj.fileName);			      
					    var fileLink = fileCallPath.replace(new RegExp(/\\/g),"/");
					    
						str += "<li data-path='"+attach.uploadPath+"' data-uuid='"+attach.uuid+"' "
						str += "data-filename='"+attach.fileName+"' data-type='"+attach.fileType+"' data-gubun='"+attach.gubun+"' >";
						str += "<span> "+ attach.fileName+"</span><br/>";
						str += "<button type='button' class='imgDeleteBtn' data-file=\'"+fileCallPath+"\' data-type='file' ";
						str += "><i class='fa fa-times'></i></button>";
						str += "<img src='/resources/img/attach.png'></a>";
						str +"</li>";
				    }
				    
				    if(attach.gubun == "profile"){
						$("#profile_result").html(str);
						$(".hidden_li_box").append(str);
						str = "";
				    }else if(attach.gubun == "carrier"){
						$(".carrier_ul").html(str);
						$(".hidden_li_box").append(str);
						str = "";
						$("#carrier_text").val(attach.fileName);
				  	  
				    }else if(attach.gubun == "cmp_reg_image"){
						$(".cmp_reg_image_ul").html(str);
						$(".hidden_li_box").append(str);
						str = "";
						$("#cmp_reg_image_text").val(attach.fileName);
				    }
			    });
			});//end getJSON
		})();//end funciton
		
		$("#listBtn").on("click", function(){
			$("#updateForm").attr("action","/insaListForm.do").attr("method","get");
			var pageNumTag = $("input[name='pageNum']").clone();
			var amountTag = $("input[name='amount']").clone();
			
			$("#updateForm").empty();
			$("#updateForm").append(pageNumTag);
			$("#updateForm").append(amountTag);
			$("#updateForm").submit();
		});
		
		$("#job_type option").each(function(){
			if($(this).val()=="<c:out value='${getInsa.job_type}' />"){
				$(this).attr("selected","selected");
			}
		});
		$("#sex option").each(function(){
			if($(this).val()=="<c:out value='${getInsa.sex}' />"){
				$(this).attr("selected","selected");
			}
		});
		$("#dept_code option").each(function(){
			if($(this).val()=="<c:out value='${getInsa.dept_code}' />"){
				$(this).attr("selected","selected");
			}
		});
		$("#join_gbn_code option").each(function(){
			if($(this).val()=="<c:out value='${getInsa.join_gbn_code}' />"){
				$(this).attr("selected","selected");
			}
		});
		$("#pos_gbn_code option").each(function(){
			if($(this).val()=="<c:out value='${getInsa.pos_gbn_code}' />"){
				$(this).attr("selected","selected");
			}
		});
		$("#gart_level option").each(function(){
			if($(this).val()=="<c:out value='${getInsa.gart_level}' />"){
				$(this).attr("selected","selected");
			}
		});
		$("#put_yn option").each(function(){
			if($(this).val()=="<c:out value='${getInsa.put_yn}' />"){
				$(this).attr("selected","selected");
			}
		});
		$("#mil_yn option").each(function(){
			if($(this).val()=="<c:out value='${getInsa.mil_yn}' />"){
				$(this).attr("selected","selected");
			}
		});
		$("#mil_type option").each(function(){
			if($(this).val()=="<c:out value='${getInsa.mil_type}' />"){
				$(this).attr("selected","selected");
			}
		});
		$("#mil_level option").each(function(){
			if($(this).val()=="<c:out value='${getInsa.mil_level}' />"){
				$(this).attr("selected","selected");
			}
		});
		$("#kosa_reg_yn option").each(function(){
			if($(this).val()=="<c:out value='${getInsa.kosa_reg_yn}' />"){
				$(this).attr("selected","selected");
			}
		});
		$("#kosa_class_code option").each(function(){
			if($(this).val()=="<c:out value='${getInsa.kosa_class_code}' />"){
				$(this).attr("selected","selected");
			}
		});
		
		
		$("#insaDeleteBtn").click(function(){
			if(confirm("정말 삭제하시겠습니까?")){
				$("#updateForm").attr("action","/insaDeleteForm.do");
				$("#updateForm").submit(); 	
			}
		});
	});
	
	</script>
</body>
</html>