
$(document).ready(function(){
	
	// submit 기본 이벤트 막고 hidden태그 생성  ==================================================================
	$("#submit_btn").on("click",function(e){
		e.preventDefault();
		
		// 이름 체크 =====================================================
	    if(document.getElementById("name").value==""){
	        alert("이름은 필수 입력사항입니다.");
	        document.getElementById("name").focus();
	        return false;
	    }
	    
		// 아이디 체크 =====================================================
		if(document.getElementById("id").value==""){
	        alert("아이디는 필수 입력사항입니다.");
	        document.getElementById("id").focus();
	        return false;
	    }
		// 비밀번호 체크=====================================================
	    if(document.getElementById("pwd").value==""){
	        alert("비밀번호는 필수 입력사항입니다.");
	        document.getElementById("pwd").focus();
	        return false;
	    }
	    // 비밀번호확인 체크 =================================================
	    if(document.getElementById("pwd_check").value==""){
	        alert("비밀번호를 체크해주세요");
	        document.getElementById("pwd_check").focus();
	        return false;
	    }
	    
	    // 연락처 체크 =====================================================
	    if(document.getElementById("hp").value==""){
	        alert("연락처는 필수 입력사항입니다.");
	        document.getElementById("hp").focus();
	        return false;
	    }
	    
	    // mil_yn disabled 풀어주고 넘기기 ==================================
	    if($("#mil_yn").val() == "no" || $("#mil_yn").val() == ""){
	    	$("#mil_type, #mil_level, #mil_startdate, #mil_enddate").removeAttr("readonly");
			$("#mil_type, #mil_level, #mil_startdate, #mil_enddate").removeAttr("disabled");
	    	$("#mil_type, #mil_level, #mil_startdate, #mil_enddate").val("");
	    }
	    
	    // mil_yn disabled 풀어주고 넘기기 ==================================
	    if($("#kosa_reg_yn").val() == "no" || $("#kosa_reg_yn").val() == ""){
	    	$("#kosa_class_code").removeAttr("readonly");
			$("#kosa_class_code").removeAttr("disabled");
	    	$("#kosa_class_code").val("");
	    }
	    
	    
	    // 프로필 이미지 타입 hidden태그로 넘기기 ===============================
	    console.log("submit clicked");	    
	    var str = "";
	    
	    // 이미지 타입  hidden태그로 넘기기 =======================================
	    $(".hidden_li_box li").each(function(i, obj){
		      
			var jobj = $(obj);
			
			console.dir(jobj);
			console.log("-------------------------");
			console.log(jobj.data("filename"));
			
			
			str += "<input type='hidden' name='attachList["+i+"].fileName' value='"+jobj.data("filename")+"'>";
			str += "<input type='hidden' name='attachList["+i+"].uuid' value='"+jobj.data("uuid")+"'>";
			str += "<input type='hidden' name='attachList["+i+"].uploadPath' value='"+jobj.data("path")+"'>";
	      	str += "<input type='hidden' name='attachList["+i+"].fileType' value='"+ jobj.data("type")+"'>";
	      	str += "<input type='hidden' name='attachList["+i+"].gubun' value='"+ jobj.data("gubun")+"'>";
	    });
	    
	    console.log(str);
	    
	    $("#insertForm").attr("action","/insaInputForm.do");
	    $("#insertForm").append(str).submit();
	});

	// 파일 확장자 체크하는 함수  ==================================================================
	var regex = new RegExp("(.*?)\.(exe|sh|zip|alz)$");
	var maxSize = 5242880; // 5MB
	
	function checkExtension(fileName, fileSize){
		if(fileSize >= maxSize){
			alert("5MB 이상의 파일은 업로드 할 수 없습니다");
			return false;
		}
		if(regex.test(fileName)){
			alert("해당 종류의 파일은 업로드 할 수 없습니다");
			return false;
		}
		return true;
	}
	
	    
	// input file에 변화가 생기면( 파일이 등록되면 )  ==============================================
		 
	$(".uploadResult").on("dragenter dragover", function(e){
		e.preventDefault();
	});
	$(".uploadResult").on("drop", function(e){
		e.preventDefault();
		
		var formData = new FormData();
		// ★★★ 파일을 드래그로 가져오는 코드 =======================================
		var files = e.originalEvent.dataTransfer.files;
		
		for(var i=0;i<files.length;i++){
			if(!checkExtension(files[i].name, files[i].size)){
				return false;
			}
			formData.append("uploadFile", files[i]);
		}
		// Ajax ================================================================================
		$.ajax({
			url: '/uploadAjaxAction',
			processData: false, 
			contentType: false,
			data:formData,
			type: 'POST',
			dataType:'json',
				success: function(result){
				console.log(result); 
				showUploadResult(result); //업로드 결과 처리 함수 
			}
		});
	});
	
	
	$("input[type='file']").change(function(){
		var formData = new FormData();
		
		var inputFile = $("input[name='uploadFile']");
		
		var files = inputFile[0].files;
		
		for(var i=0;i<files.length;i++){
			if(!checkExtension(files[i].name, files[i].size)){
				return false;
			}
			formData.append("uploadFile", files[i]);
		}
		// Ajax ================================================================================
		$.ajax({
			url: '/uploadAjaxAction',
			processData: false, 
			contentType: false,
			data:formData,
			type: 'POST',
			dataType:'json',
				success: function(result){
				console.log(result); 
				showUploadResult(result); //업로드 결과 처리 함수 
			}
		});
		
	});
	
	// profile btn 프로필사진 버튼 파일대신 작동 , 버튼값 넣기  =========================================
	var clickBtn = "";
	$("#profile,#carrier,#cmp_reg_image").click(function(e){
		e.preventDefault();
		clickBtn = $(this).attr("id");
	    console.log("클릭된 파일버튼 구분값 찾기"+clickBtn);
	    
		$("input[type='file']").click();
	});
	
	// showUploadResult(); 함수 생성  ===========================================================
	function showUploadResult(uploadResultArr){
		
	    if(!uploadResultArr || uploadResultArr.length == 0){ return; }
	    
	    var uploadUL = $("#profile_result");
	    
	    var str ="";
	    
	    $(uploadResultArr).each(function(i, obj){
			if(obj.image){
				var fileCallPath =  encodeURIComponent( obj.uploadPath+ "/s_"+obj.uuid +"_"+obj.fileName);
				str += "<li data-path='"+obj.uploadPath+"'";
				str += " data-uuid='"+obj.uuid+"' data-filename='"+obj.fileName+"' data-type='"+obj.image+"' data-gubun='"+clickBtn+"' >";
				str += "<span> "+ obj.fileName+"</span>";
				str += "<button type='button' title='삭제' class='imgDeleteBtn' data-file=\'"+fileCallPath+"\' ";
				str += "data-type='image'><i class='fa fa-times'></i></button>";
				str += "<img src='/display?fileName="+fileCallPath+"'>";
				str +"</li>";
			}else{
				var fileCallPath =  encodeURIComponent( obj.uploadPath+"/"+ obj.uuid +"_"+obj.fileName);			      
			    var fileLink = fileCallPath.replace(new RegExp(/\\/g),"/");
			      
				str += "<li "
				str += "data-path='"+obj.uploadPath+"' data-uuid='"+obj.uuid+"' data-filename='"+obj.fileName+"' data-type='"+obj.image+"' >";
				str += "<span> "+ obj.fileName+"</span>";
				str += "<button type='button' class='imgDeleteBtn' data-file=\'"+fileCallPath+"\' data-type='file' " 
				str += "><i class='fa fa-times'></i></button>";
				str += "<img src='/resources/img/attach.png'></a>";
				str +"</li>";
			}
	    });
	    
	    
	    if(clickBtn == "profile"){ // 클릭된 버튼이 프로필이미지 등록 버튼이면 ======================
	    	uploadUL.append(str);
	    	$(".hidden_li_box").append(str);
	    }else if(clickBtn == "cmp_reg_image"){  // 클릭된 버튼이 사업자 등록증 등록 버튼이면==============
	    	$("ul.cmp_reg_image_ul").append(str);
	    	$(".hidden_li_box").append(str);
	    	// 사업자 등록증 이름 노출해주기
	    	$("ul.cmp_reg_image_ul li").each(function(i, obj){
				var jobj = $(obj);
				$("#cmp_reg_image_text").val(jobj.data("filename"));
		    });
	    }else if(clickBtn == "carrier"){ // 클릭된 버튼이 이력서 등록 버튼이면 ========================
	    	$("ul.carrier_ul").append(str);
	    	$(".hidden_li_box").append(str);
	    	// 이력서 이름 노출해주기
	    	$("ul.carrier_ul li").each(function(i, obj){
				var jobj = $(obj);
				$("#carrier_text").val(jobj.data("filename"));
		    });
	    }
	}
	
	$("#profile_result,ul.cmp_reg_image_ul,ul.carrier_ul").on("click", ".imgDeleteBtn", function(e){
	    console.log("delete file");
	      
	    var targetFile = $(this).data("file");
	    var type = $(this).data("type");
	    
	    var targetLi = $(this).closest("li");

	    // 이름 지워주는 함수 추가 ================== 
	    $.ajax({
	      url: '/deleteFile',
	      data: {fileName: targetFile, type:type},
	      dataType:'text',
	      type: 'POST',
	        success: function(result){
	           alert(result);
	           targetLi.remove();
	         }
	    }); //$.ajax
	 });
});