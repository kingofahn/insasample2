/**
 * 
 * insaInputForm.jsp를 위한 javascript / jquery
 * 
 */

$( function() {
	// 날짜 체크 ===========================
	$("#join_day,#retire_day").blur(function(){
		console.log("join_day");
		if($(this).attr("id") == "join_day"){
			console.log("join_day");
		}else if($(this).attr("id") == "retire_day"){
			console.log("retire_day");
		}
	});
	$( "#retire_day,#join_day,#mil_startdate,#mil_enddate" ).datepicker({
		changeMonth: true, // 월을 바꿀수 있는 셀렉트 박스를 표시한다.
		changeYear: true, // 년을 바꿀 수 있는 셀렉트 박스를 표시한다.
		nextText: '다음 달', // next 아이콘의 툴팁.
		prevText: '이전 달', // prev 아이콘의 툴팁.
		dateFormat: "yy-mm-dd", // 텍스트 필드에 입력되는 날짜 형식.
		showMonthAfterYear: true , // 월, 년순의 셀렉트 박스를 년,월 순으로 바꿔준다. 
		dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'], // 요일의 한글 형식.
		monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'] // 월의 한글 형식.
	});
});

$(document).ready(function(){
	
	// name,eng_name,id,pwd validation 필수 입력값 확인================
	$("input#name,input#id,input#pwd, input#email").blur(function(){
		if(this.value==""){
			$(this).parent("div").parent(".padding").addClass("on");
		}else{
			$(this).parent("div").parent(".padding").removeClass("on");
		}
    });
	
	// pwd_check validation 비밀번호 확인체크
	$("input#pwd_check").blur(function(){
		var pwTxt=document.getElementById("pwd").value;
		var pwchkTxt=document.getElementById("pwd_check").value;
		
		if(this.value==""){
			$(this).parent("div").parent(".padding").addClass("on");
		}else{
			if(pwTxt==pwchkTxt){
				$(this).parent("div").parent(".padding").removeClass("on");	
			}else{
				$(this).parent("div").parent(".padding").addClass("on");	
			}
		}
    });
	
	// hp, phone validation 전화번호 형식 체크 ========================
	$("input#hp, input#phone").blur(function(){
		var phNumTxt=$(this).val();
		var firstNum=phNumTxt.substring(0,3);
		console.log(firstNum);
		if(this.value==""){
			if($(this).attr("id")=="hp"){
				$(this).parent("div").parent(".padding").addClass("on");
				$(this).parent("div").parent(".padding").find("> .warning_text p").text("");
				$(this).parent("div").parent(".padding").find("> .warning_text p").text("전화번호는 필수 입력값입니다.");	
			}
		}else{
			if(firstNum=="010"||firstNum=="011"||firstNum=="016"||firstNum=="017"||firstNum=="018"){
				$(this).parent("div").parent(".padding").removeClass("on");	
			}else{
				$(this).parent("div").parent(".padding").find("> .warning_text p").text("");
				$(this).parent("div").parent(".padding").find("> .warning_text p").text("정확한 전화번호를 입력해주세요");
				$(this).parent("div").parent(".padding").addClass("on");	
			}
		}
    });
	
	//mil_yn check 군필 미필 여부에 따라 입력할수 있게
	$("#mil_type, #mil_level, #mil_startdate, #mil_enddate").attr("disabled","disabled");
	
	$("#mil_yn").change(function(){
		if($("#mil_yn").val() == "yes"){
			$("#mil_type, #mil_level, #mil_startdate, #mil_enddate").val("");
			$("#mil_type, #mil_level, #mil_startdate, #mil_enddate").removeAttr("readonly");
			$("#mil_type, #mil_level, #mil_startdate, #mil_enddate").removeAttr("disabled");
		}else if($("#mil_yn").val() == "no"){
			$("#mil_type, #mil_level, #mil_startdate, #mil_enddate").val("");
			$("#mil_type, #mil_level, #mil_startdate, #mil_enddate").attr("readonly",true);
			$("#mil_type, #mil_level, #mil_startdate, #mil_enddate").attr("disabled",true);
		}
	});
	
	// kosa_reg_yn check 코사등록 여부에 따라 코사등록번호 입력할수 있게
	$("#kosa_class_code").attr("disabled","disabled"); 
	$("#kosa_reg_yn").change(function(){
		if($("#kosa_reg_yn").val() == "yes"){
			$("#kosa_class_code").val("");
			$("#kosa_class_code").removeAttr("readonly");
			$("#kosa_class_code").removeAttr("disabled");
		}else if($("#kosa_reg_yn").val() == "no"){
			$("#kosa_class_code").val("");
			$("#kosa_class_code").attr("readonly",true);
			$("#kosa_class_code").attr("disabled",true);
		}
	});
	
	// 주민번호 체크 ===========================
	var d = new Date();
	$("#reg_no").blur(function(){
		var reg_no = $("#reg_no").val();
		var bornIn = "";
		var years = 0;
		var sex="";

		if(reg_no != ""){// reg_no가 입력값이 있을때
			// 나이 자동 입력 ====
			if(reg_no.substring( 0, 1 ) == "0" || reg_no.substring( 0, 1 ) == "1"){
				bornIn += "20"; 
			}else{
				bornIn += "19"; 
			}
			bornIn += reg_no.substring( 0, 2 ); 
			years = d.getFullYear() - Number(bornIn) + 1;
			$("input#years").val();
			$("input#years").val(years);
			$("input#years").attr("readonly", true);
			
			// 성별 자동 입력 ====
			if(reg_no.substring( 6, 7 ) == "1" || reg_no.substring( 6, 7 ) == "3"){
				$("#sex").val("1");
			}else{
				$("#sex").val("0");
			}
		}
	});
});

// daum adress api사용하기 ====================================
function sample6_execDaumPostcode() {
    new daum.Postcode({
        oncomplete: function(data) {
            // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

            // 각 주소의 노출 규칙에 따라 주소를 조합한다.
            // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
            var addr = ''; // 주소 변수
            var extraAddr = ''; // 참고항목 변수

            //사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
            if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                addr = data.roadAddress;
            } else { // 사용자가 지번 주소를 선택했을 경우(J)
                addr = data.jibunAddress;
            }

            // 사용자가 선택한 주소가 도로명 타입일때 참고항목을 조합한다.
            if(data.userSelectedType === 'R'){
                // 법정동명이 있을 경우 추가한다. (법정리는 제외)
                // 법정동의 경우 마지막 문자가 "동/로/가"로 끝난다.
                if(data.bname !== '' && /[동|로|가]$/g.test(data.bname)){
                    extraAddr += data.bname;
                }
                // 건물명이 있고, 공동주택일 경우 추가한다.
                if(data.buildingName !== '' && data.apartment === 'Y'){
                    extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                // 표시할 참고항목이 있을 경우, 괄호까지 추가한 최종 문자열을 만든다.
                if(extraAddr !== ''){
                    extraAddr = ' (' + extraAddr + ')';
                }
                // 조합된 참고항목을 해당 필드에 넣는다.
                addr += extraAddr;
            
            }

            // 우편번호와 주소 정보를 해당 필드에 넣는다.
            document.getElementById("zip").value = data.zonecode;
            document.getElementById("addr1").value = addr;
            // 커서를 상세주소 필드로 이동한다.
            document.getElementById("addr2").focus();
            $("#zip").attr("readonly",true);
        }
    }).open();
}
// pwd, pwd_check, reg_no 마지막 글자 평문으로 보이게
/* function insertPass(){
	var a = document.getElementById("pwd_box");
	var b = document.getElementById("pwd");
	var c = "";
	
	if(a.value.substring(a.value.length-1,a.value.length)=='*'){
		b.value = b.value.substring(0,b.value.length-1);
	}else if(a.value.substring(a.value.length-1,a.value.length)==""){
		b.value = b.value.substring(0,b.value.length-1);
	}else{
		b.value = b.value + a.value.substring(a.value.length-1,a.value.length);
	}
	for(i=0;i<a.value.length-1;i++){
		c = c+"*";
	}
	c = c+a.value.substring(a.value.length-1,a.value.length);
	a.value = c;
	
	console.log("pwd_box의 value는 ============> " + $("#pwd_box").val());
	console.log("pwd의 value는 ============> " + $("#pwd").val());
	console.log("c =======> " + c);
	
} */

// hp autoHypen 연락처 자동 하이픈추가 ======================
function autoHypenHp() {
	var x = document.getElementById("hp");
	x.value = x.value.replace(/[^0-9]/g, '');
	var tmp = "";

	if (x.value.length > 3 && x.value.length <= 7) {
		tmp += x.value.substr(0, 3);
		tmp += '-';
		tmp += x.value.substr(3);
		x.value = tmp;
	} else if (x.value.length > 7) {
		tmp += x.value.substr(0, 3);
		tmp += '-';
		tmp += x.value.substr(3, 4);
		tmp += '-';
		tmp += x.value.substr(7);
		x.value = tmp;
	}
}
// phone autoHypen 연락처 자동 하이픈추가 ======================
function autoHypenPhone() {
	var x = document.getElementById("phone");
	x.value = x.value.replace(/[^0-9]/g, '');
	var tmp = "";

	if (x.value.length > 3 && x.value.length <= 7) {
		tmp += x.value.substr(0, 3);
		tmp += '-';
		tmp += x.value.substr(3);
		x.value = tmp;
	} else if (x.value.length > 7) {
		tmp += x.value.substr(0, 3);
		tmp += '-';
		tmp += x.value.substr(3, 4);
		tmp += '-';
		tmp += x.value.substr(7);
		x.value = tmp;
	}
}
