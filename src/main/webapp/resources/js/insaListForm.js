$(document).ready(function () {
	var actionForm = $("#actionForm");
	
    $("#jqGrid").jqGrid({
        url: '/list',
        mtype: "GET",
        datatype: "json",
        colModel: [
            { label: '사번', name: 'sabun', key: true, width: 75 },
            { label: '성명', name: 'name', width: 150 },
            { label: '주민번호', name: 'reg_no', width: 150 },
            { label: '연락처', name: 'hp', width: 150 },
            { label: '직위', name: 'pos_gbn_code', width: 150 
            	, formatter: function(cellValue, options, rowObject){
            		var resultStr = "";
    	  			if(cellValue == "0001"){
    	  				resultStr = "사원";        	  				
    	  			}else if(cellValue== "0002"){
    	  				resultStr = "대리";
    	  			}else if(cellValue== "0003"){
    	  				resultStr = "과장";
    	  			}else if(cellValue== "0004"){
    	  				resultStr = "차장";
    	  			}else if(cellValue== "0005"){
    	  				resultStr = "부장";
    	  			}else if(cellValue== "0006"){
    	  				resultStr = "사장";
    	  			}else{
    	  				resultStr = cellValue;
    	  			}
    	  			return resultStr;
            	} 
            },
            { label: '입사일', name: 'join_day', width: 150 
            	, formatter: function(cellValue, options, rowObject){
            		if(cellValue != null){
            			var resultStr = "";
                		resultStr = cellValue.substring( 0, 10 );
        	  			return resultStr;	
            		}
            	}
            },
            { label: '퇴사일', name: 'retire_day', width: 150 
            	, formatter: function(cellValue, options, rowObject){
            		if(cellValue != null){
            			var resultStr = "";
                		resultStr = cellValue.substring( 0, 10 );
        	  			return resultStr;	
            		}else{
            			var resultStr = "-";
        	  			return resultStr;
            		}
            	}
            },
            { label: '투입여부', name: 'put_yn', width: 150
            	, formatter: function(cellValue, options, rowObject){
            		var resultStr = "";

    	  			if(cellValue == "0"){
    	  				resultStr = "가능";        	  				
    	  			}else if(cellValue== "1"){
    	  				resultStr = "불가능";
    	  			}else{
    	  				resultStr = cellValue;
    	  			}
    	  			return resultStr;
            	} 
            },
            { label: '연봉', name: 'salary', width: 100 }
        ],
        viewrecords: true,
        height: 450,
        rowNum: 10,
        rowList:[10,20,30],
        pager: "#jqGridPager",
        sortable: true, 
        loadonce : true,
        onCellSelect: function(rowid, index, content, event){
        	var sabun = $(this).jqGrid('getCell', rowid, 'sabun');

        	actionForm.append("<input type='hidden' name='sabun' value='"+ sabun +"' />");
        	actionForm.attr("action","/insaUpdateForm.do");
        	actionForm.submit();
        },
        loadError: function(xhr,st,err){
        	alert("조건에 맞는 데이터가 없습니다.");
        	window.location.reload();
        },
        loadComplete: function(data) {
			console.log(data);
		}
    });
});

$("#searchBtn").on("click",function(){
	rowData = null;
	var sabun = $("#sabun").val();
	var name = $("#name").val();
	var join_gbn_code = $("#join_gbn_code").val();
	var put_yn = $("#put_yn").val();
	var pos_gbn_code = $("#pos_gbn_code").val();
	var join_day = $("#join_day").val();
	var retire_day = $("#retire_day").val();
	var job_type = $("#job_type").val();
	
	var postData = {};
	
	if($("#sabun").val() != ""){
		postData.sabun = sabun;
	}
	if($("#name").val() != ""){
		postData.name = name;
	}
	if($("#join_gbn_code").val() != ""){
		postData.join_gbn_code = join_gbn_code;
	}
	if($("#put_yn").val() != ""){
		postData.put_yn = put_yn;
	}
	if($("#pos_gbn_code").val() != ""){
		postData.pos_gbn_code = pos_gbn_code;
	}
	if($("#join_day").val() != ""){
		postData.join_day = join_day;
	}
	if($("#retire_day").val() != ""){
		postData.retire_day = retire_day;
	}
	if($("#job_type").val() != ""){
		postData.job_type = job_type;
	}
	
	console.log(postData);
	
	$("#jqGrid").jqGrid("clearGridData", true);
	
	$("#jqGrid").setGridParam({
		datatype: "json",
		postData: postData, 
		loadComplete: function(data) {
			console.log(data);
			if(data.length == 0){
				alert("조건에 맞는 데이터가 없습니다.");
	        	window.location.reload();
			}
		}
	}).trigger("reloadGrid");
});

/* $("#searchBtn").click(function(){
	var sabun = $("#sabun").val();
	var name = $("#name").val();
	var join_gbn_code = $("#join_gbn_code").val();
	var put_yn = $("#put_yn").val();
	var pos_gbn_code = $("#pos_gbn_code").val();
	var join_day = $("#join_day").val();
	var retire_day = $("#retire_day").val();
	var job_type = $("#job_type").val();
	
	if(sabun == ""){
		$("#sabun").parent("div").empty();
	}
	if(name == ""){
		$("#name").parent("div").empty();
	}
	if(join_gbn_code == ""){
		$("#join_gbn_code").parent("div").empty();
	}
	if(put_yn == ""){
		$("#put_yn").parent("div").empty();
	}
	if(pos_gbn_code == ""){
		$("#pos_gbn_code").parent("div").empty();
	}
	if(join_day == ""){
		$("#join_day").parent("div").empty();
	}
	if(retire_day == ""){
		$("#retire_day").parent("div").empty();
	}
	if(job_type == ""){
		$("#job_type").parent("div").empty();
	}
	
	$("#listForm").attr("action", "/insaListForm.do");
	$("#listForm").submit();
});  */

$(document).ready(function() {
	var nowPage = '<c:out value="${ pageMaker.cri.pageNum }" />';
	
	$( ".pagging_nav li" ).each(function( index ) {
		if($(this).find("a").attr("href") == nowPage){
			$(this).find("a").addClass("on");
		}
	});
	console.log(nowPage);
	// 상세보기 버튼 클릭 ============================================
	$(".move").on("click",function(e){
		e.preventDefault();
		actionForm.append("<input type='hidden' name='sabun' value='"+ $(this).attr("href") +"' />");
		actionForm.attr("action","/insaUpdateForm.do");
		actionForm.submit();
	});
	// 페이징 추가 =================================================
	var actionForm = $("#actionForm");
	
	$(".pagging_nav a").on("click", function(e){
		e.preventDefault();
		
		actionForm.find("input[name='pageNum']").val($(this).attr("href"));
		actionForm.submit();
	});
	
	
	
	//history 객체 초기화
	history.replaceState({},null,null);
	
	//result가 없으면 중지
	/* if(result === '' || history.state){
		return;
	}
	if(result != "" || result != null){
		alert(result);
	}
	if(parseInt(result) > 0){
		alert(result);
	} */
	
	
	/* $("#searchBtn").click(function(){
		var sabun = $("#sabun").val();
		var name = $("#name").val();
		var join_gbn_code = $("#join_gbn_code").val();
		var put_yn = $("#put_yn").val();
		var pos_gbn_code = $("#pos_gbn_code").val();
		var join_day = $("#join_day").val();
		var retire_day = $("#retire_day").val();
		var job_type = $("#job_type").val();
		
		if(sabun == ""){
			$("#sabun").parent("div").empty();
		}
		if(name == ""){
			$("#name").parent("div").empty();
		}
		if(join_gbn_code == ""){
			$("#join_gbn_code").parent("div").empty();
		}
		if(put_yn == ""){
			$("#put_yn").parent("div").empty();
		}
		if(pos_gbn_code == ""){
			$("#pos_gbn_code").parent("div").empty();
		}
		if(join_day == ""){
			$("#join_day").parent("div").empty();
		}
		if(retire_day == ""){
			$("#retire_day").parent("div").empty();
		}
		if(job_type == ""){
			$("#job_type").parent("div").empty();
		}
		
		$("#listForm").attr("action", "/insaListForm.do");
		$("#listForm").submit();
	});  */
});